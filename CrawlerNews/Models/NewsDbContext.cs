﻿using Microsoft.EntityFrameworkCore;

namespace CrawlerNews.Models
{
    public class NewsDbContext : DbContext
    {
        public NewsDbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.;Database=NewsDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public DbSet<News> News { get; set; }
    }
}
