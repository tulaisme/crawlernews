﻿using CrawlerNews.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrawlerNews.Controllers
{
    public class SearchController : Controller
    {
        private readonly INewsServices _newsServices;
        public SearchController(INewsServices newsServices)
        {
            _newsServices = newsServices;
        }
        public IActionResult Index(string q = null, int page = 1)
        {
            if(q == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var news = _newsServices.SearchPaging(q, page, 24);
            return View("Index", news);
        }
    }
}
