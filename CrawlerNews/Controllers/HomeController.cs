﻿using CrawlerNews.Models;
using CrawlerNews.Services.Interface;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CrawlerNews.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly INewsServices _newsServices;
        private readonly ICrawlerServices _crawlerServices;

        public HomeController(ILogger<HomeController> logger, INewsServices newsServices, ICrawlerServices crawlerServices)
        {
            _logger = logger;
            _newsServices = newsServices;
            _crawlerServices = crawlerServices;
        }

        public IActionResult Index()
        {
            ViewData["count"] = _newsServices.Count();
            var news = _newsServices.GetNewsPaging(1, 24);
            return View(news);
        }

        public IActionResult LoadMore(int page = 1)
        {
            var news = _newsServices.GetNewsPaging(page, 24);
            return Json(news);
        }

        

        public ActionResult<string> StartCrawler()
        {
            RecurringJob.AddOrUpdate(() => _crawlerServices.Crawler(), "0 */5 * ? * *");
            return Ok($"Recurring Job Scheduled. Crawler will be start daily!");
            //int result = _crawlerServices.Crawler();
            //if (result > 0)
            //{
            //    return $"Crawled {result} items";
            //}
            //return "Nothing was crawled";
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
