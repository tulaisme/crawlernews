﻿using CrawlerNews.Models;
using CrawlerNews.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrawlerNews.Services
{
    public class NewsServices:INewsServices
    {
        private readonly NewsDbContext _context;
        public NewsServices()
        {
            _context = new NewsDbContext();
        }
        public IEnumerable<News> GetNews()
        {
            var news = _context.News.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate).ToList();
            return news;
        }

        public void Add(News news)
        {
            _context.Add(news);
        }

        public News Detail(string slug)
        {
            return _context.News.FirstOrDefault(x => x.Slug.Equals(slug) && !x.IsDeleted);
        }

        public bool IsExisted(string slug)
        {
            return _context.News.FirstOrDefault(x => x.Slug.Equals(slug)) != null;
        }

        public int Count()
        {
            return _context.News.Count();
        }

        public IEnumerable<News> GetNewsPaging(int page = 1, int size = 10)
        {
            var news = _context.News
                .Where(x => !x.IsDeleted)
                .OrderByDescending(x => x.CreatedDate)
                .Skip((page - 1) * size)
                .Take(size)
                .ToList();
            return news;
        }

        public IEnumerable<News> SearchPaging(string query, int page = 1, int size = 10)
        {
            var news = _context.News
                .Where(x => x.Title.ToLower().Contains(query.ToLower()) && !x.IsDeleted)
                .OrderByDescending(x => x.CreatedDate)
                .Skip((page - 1) * size)
                .Take(size)
                .ToList();
            return news;
        }

        public bool Save()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
