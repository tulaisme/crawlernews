﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrawlerNews.Services.Interface
{
    public interface ICrawlerServices
    {
        int Crawler();
    }
}
