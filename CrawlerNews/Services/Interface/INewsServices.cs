﻿using CrawlerNews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrawlerNews.Services.Interface
{
    public interface INewsServices
    {
        IEnumerable<News> GetNews();
        void Add(News news);
        News Detail(string slug);
        IEnumerable<News> GetNewsPaging(int page, int size);
        IEnumerable<News> SearchPaging(string query, int page, int size);
        bool Save();
        bool IsExisted(string slug);
        int Count();
    }
}
