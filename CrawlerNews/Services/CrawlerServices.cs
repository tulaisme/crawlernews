﻿using CrawlerNews.Models;
using CrawlerNews.Services.Interface;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerNews.Services
{
    public class CrawlerServices : ICrawlerServices
    {
        private readonly INewsServices _newsServices;
        public CrawlerServices(INewsServices newsServices)
        {
            _newsServices = newsServices;
        }
        public int Crawler()
        {
            HtmlWeb htmlWeb = new HtmlWeb()
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8
            };

            //Load trang web, nạp html vào document
            HtmlDocument document = htmlWeb.Load("https://www.vnexpress.net");

            int count = 0;
            var articles = document.DocumentNode.Descendants("article").Where(node => node.HasClass("item-news")).ToList();
            foreach (var article in articles)
            {
                var newsTitle = "";
                var newsThumb = "";
                var newsLink = "";
                var newsDesc = "";

                var titleNode = article.Descendants("h3").Where(node => node.HasClass("title-news")).FirstOrDefault()
                    .Descendants("a").FirstOrDefault();
                if(titleNode != null)
                {
                    newsTitle = titleNode.InnerHtml;
                    newsLink = titleNode.Attributes["href"].Value;
                }
                
                var picture = article.Descendants("picture").FirstOrDefault();
                if(picture != null)
                {
                    newsThumb = picture.InnerHtml;
                }

                var descriptionNode = article.Descendants("p").Where(node => node.HasClass("description")).FirstOrDefault();
                if(descriptionNode != null)
                {
                    newsDesc = descriptionNode.Descendants("a").FirstOrDefault().InnerHtml;
                }

                News news = new News()
                {
                    ID = Guid.NewGuid(),
                    Title = newsTitle,
                    Slug = GetSlug(newsTitle),
                    Link = newsLink,
                    ShortContent = newsDesc,
                    Content = newsDesc,
                    Thumbnail = newsThumb,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsDeleted = false
                };

                var isExsisted = _newsServices.IsExisted(news.Slug);
                if (!isExsisted)
                {
                    _newsServices.Add(news);
                    count++;
                }
            }

            if(count == 0 || !_newsServices.Save())
                count = 0;
            return count;
        }


        private string GetSlug(string title)
        {
            return title.ToLower().Trim().Replace(" ","-");
        }
    }
}
